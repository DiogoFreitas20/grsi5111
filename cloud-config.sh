#!/bin/bash -x
DOMAIN=enta.com
# Reference: https://serverfault.com/questions/103501/how-can-i-fully-log-all-bash-scripts-actions
# Log everything
#

echo ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCaxG8yfjOL+jojS6iclM6kjOwxJL4wfhaNj+dmD9xMN/G4VGvZIFL0wWei/uanMTOYMOuUEOsdirWYgRmmW4z+X/SgGHpsGozm+sUnNn6SQos1mrkjXoEqhy6M5tqX3xoIvlw3cTmCUmEgfRVzHTCtu9jOEmwQA2WbPoYrS+0SQ9JWKKq4+7v7bfElQzDzlGcIqoP8kd/TSQkIU3zgDcXmh1kA6iwewYiGGvX+GiMGBUqVxQAha9BkaMJc4NtBC5A9xWqerY39XiYvIqvIbmNL/qxO9se4ArgK3kHGfHjfKRSsaCDMWC/fn3imA0kT6pATfZn/IImNoGwiqBxUF2tn GRSI-KEY >> ~/.ssh/authorized_keys

exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>/var/log/cloud-config-detail.log 2>&1

wget https://gist.githubusercontent.com/jdmedeiros/1add075e054c911776d26e97a84dfdec/raw/62c0ccd0e8d76e0dc9d1b15fe369d101dd7bc12f/logger.sh
source ./logger.sh

SCRIPTENTRY
DEBUG "Script starting."
sudo hostnamectl set-hostname teste
sudo sh -c 'echo root:Passw0rd | chpasswd'
sudo apt-get update && sudo apt-get -y upgradecloud-config.sh
INFO "System updated and upgraded."

sudo echo "iptables-persistent    iptables-persistent/autosave_v4 boolean true" | debconf-set-selections
sudo echo "iptables-persistent    iptables-persistent/autosave_v6 boolean false" | debconf-set-selections
sudo apt-get -y install iptables-persistent netfilter-persistent
sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 3389 -j DNAT --to-destination 172.21.1.11
sudo netfilter-persistent save
sudo apt-get -y install apache2
INFO "Software packages installed."
sudo a2enmod ssl
sudo a2ensite default-ssl.conf
sudo systemctl restart apache2
sudo systemctl enable apache2
sudo echo "apache porta 80" > /var/www/html/index.html
sudo systemctl restart apache2
sudo apt update && apt upgrade -y
sudo apt install bind9* -y
sudo cd /etc/bind/
sudo cp db.local db.enta.com
sudo cp db.127 db.rev.enta.com
sudo cat <<'EOF' > /etc/bind/named.conf.options
options {
        directory "/var/cache/bind";

        // If there is a firewall between you and nameservers you want
        // to talk to, you may need to fix the firewall to allow multiple
        // ports to talk.  See http://www.kb.cert.org/vuls/id/800113

        // If your ISP provided one or more IP addresses for stable
        // nameservers, you probably want to use them as forwarders.
        // Uncomment the following block, and insert the addresses replacing
        // the all-0's placeholder.

        forwarders {
                8.8.8.8;
        };

        //========================================================================
        // If BIND logs error messages about the root key being expired,
        // you will need to update your keys.  See https://www.isc.org/bind-keys
        //========================================================================
        dnssec-validation auto;

        listen-on-v6 { any; };
};
EOF
sudo cat <<'EOF' > /etc/bind/named.conf.local
zone "enta.com" {
        type master;
        file "/etc/bind/db.enta.com";
};

zone "21.172.in-addr.arpa" {
        type master;
        file "/etc/bind/db.rev.enta.com";
};
EOF
sudo sed -i -e 's/localhost\enta.com/g' /etc/bind/db.enta.com
sudo sed -i -e 's/localhost\enta.com/g' /etc/bind/db.rev.enta.com
sudo cat <<'EOF' > /etc/bind/db.enta.com
;
; BIND data file for local loopback interface
;
$TTL    604800
@       IN      SOA     enta.com. root.enta.com. (
                              2         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
;
@       IN      NS      enta.com.
@       IN      NS      ns1.enta.com.
@       IN      NS      ns2.enta.com.
@       IN      A       172.21.0.10
@       IN      MX      10      smtp.enta.com.
smtp    IN      A       172.21.0.10
www     IN      CNAME   enta.com.
EOF
sudo systemctl restart bind9
INFO "Web sited created. Joomla on port 80 and Moodle on port 8080."
DEBUG "Script reached the end."
SCRIPTEXIT

rm logger.sh